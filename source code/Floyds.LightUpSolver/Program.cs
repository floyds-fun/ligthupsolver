﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Floyds.LightUpSolver
{
    public static class Program
    {
        private static ApplicationContext cntx;
        private static FormSplash splashScreen;
        private static FormMain mainForm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            cntx = new ApplicationContext();
            splashScreen = new FormSplash();
            mainForm = new FormMain();

            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
                BringToFront(mainForm.Text);
            else
            {
                splashScreen.FormClosed += SplashClosed;

#if DEBUG
                cntx.MainForm = mainForm;
#else
                cntx.MainForm = splashScreen;
#endif
                
                Application.Run(cntx);
            }
        }

        private static void SplashClosed(object sender, FormClosedEventArgs e)
        {
            cntx.MainForm = mainForm;
            mainForm.Show();
        }

        private static void BringToFront(string title)
        {
            IntPtr handle = NativeMethods.FindWindow(null, title);

            if (handle != IntPtr.Zero)
                NativeMethods.SetForegroundWindow(handle);
        }
    }
}