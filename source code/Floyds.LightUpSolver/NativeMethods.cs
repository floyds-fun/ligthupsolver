﻿using System;
using System.Runtime.InteropServices;

namespace Floyds.LightUpSolver
{
    public static class NativeMethods
    {
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        internal static extern IntPtr FindWindow(String lpClassName, String lpWindowName);
        [DllImport("USER32.DLL")]
        internal static extern bool SetForegroundWindow(IntPtr hWnd);
    }
}