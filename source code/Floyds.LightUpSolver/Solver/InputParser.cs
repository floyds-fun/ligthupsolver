﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Floyds.LightUpSolver
{
    public static class InputParser
    {
        private static Color blueColor = Color.FromArgb(51, 102, 204);
        private static Color yellowColor = Color.FromArgb(255, 255, 153);
        private static Color blackColor = Color.FromArgb(0, 0, 0);
        private static Color whiteColor = Color.FromArgb(255, 255, 255);
        private static Color redColor = Color.FromArgb(204, 51, 51);

        public static sbyte[,] GetField(out int xField, out int yField)
        {
            sbyte[,] result = null;

            var field = GetFieldImage(GetScreenshot(), out xField, out yField);
            if (field != null)
            {
                int dimension = field.Width / Global.GridSize;

                result = new sbyte[dimension, dimension];

                for (int x = 0; x < dimension; x++)
                    for (int y = 0; y < dimension; y++)
                    {
                        if (field.GetPixel(x * Global.GridSize + 1, y * Global.GridSize + 1) == yellowColor && field.GetPixel(x * Global.GridSize + 6, y * Global.GridSize + 6) == whiteColor)
                            result[x, y] = FieldValue.Lamp;
                        else if (field.GetPixel(x * Global.GridSize + 1, y * Global.GridSize + 1) == yellowColor)
                            result[x, y] = FieldValue.Light;
                        else if (field.GetPixel(x * Global.GridSize + 1, y * Global.GridSize + 1) == whiteColor && field.GetPixel(x * Global.GridSize + 6, y * Global.GridSize + 6) == redColor)
                            result[x, y] = FieldValue.Cross;
                        else if (field.GetPixel(x * Global.GridSize + 1, y * Global.GridSize + 1) == whiteColor)
                            result[x, y] = FieldValue.Empty;
                        else if (field.GetPixel(x * Global.GridSize + 10, y * Global.GridSize + 12) == blackColor)
                            result[x, y] = FieldValue.Block;
                        else if (field.GetPixel(x * Global.GridSize + 6, y * Global.GridSize + 8) == whiteColor)
                            result[x, y] = FieldValue.Zero;
                        else if (field.GetPixel(x * Global.GridSize + 8, y * Global.GridSize + 6) == whiteColor)
                            result[x, y] = FieldValue.One;
                        else if (field.GetPixel(x * Global.GridSize + 6, y * Global.GridSize + 12) == whiteColor)
                            result[x, y] = FieldValue.Two;
                        else if (field.GetPixel(x * Global.GridSize + 6, y * Global.GridSize + 6) == whiteColor && field.GetPixel(x * Global.GridSize + 8, y * Global.GridSize + 8) == whiteColor)
                            result[x, y] = FieldValue.Three;
                        else if (field.GetPixel(x * Global.GridSize + 8, y * Global.GridSize + 7) == whiteColor && field.GetPixel(x * Global.GridSize + 10, y * Global.GridSize + 11) == whiteColor)
                            result[x, y] = FieldValue.Four;
                        else
                            throw new Exception("Can't parse the cell [" + x + ", " + y + "].");
                    }
            }
            else
                throw new Exception("Can't find a field.");

            return result;
        }

        private static Bitmap GetFrameImage(Bitmap image, out int xFrame, out int yFrame)
        {
            int minX = 300;
            int minY = 125;

            for (int y = minY; y < image.Height; y++)
                for (int x = minX; x < image.Width; x++)
                {
                    // checking for the blue frame
                    if (image.GetPixel(x, y) == blueColor)
                    {
                        int frameX = x;
                        int frameY = y;

                        while (image.GetPixel(frameX, y) == blueColor)
                            frameX++;
                        while (image.GetPixel(x, frameY) == blueColor)
                            frameY++;

                        int frameXLenght = frameX - x;
                        int frameYLenght = frameY - y;

                        // big enough, equal sides
                        if (frameXLenght > 100 && frameXLenght == frameYLenght)
                        {
                            // checking candidate frame for yellow pixels above - LightUp logo
                            int yellowPixelY = y - 50;
                            int yellowPixelsCount = 0;
                            int yellowPixelsThreshold = 14;

                            while (yellowPixelsCount < yellowPixelsThreshold && yellowPixelY > y - minY)
                            {
                                // checking in the middle X to get all sizes
                                if (image.GetPixel(x + frameXLenght / 2, yellowPixelY) == yellowColor)
                                    yellowPixelsCount++;

                                yellowPixelY--;
                            }

                            if (yellowPixelsCount == yellowPixelsThreshold)
                            {
                                // bingo!
                                xFrame = x;
                                yFrame = y;
                                return (new Bitmap(image)).Clone(new Rectangle(x, y, frameXLenght, frameXLenght), image.PixelFormat);
                            }
                        }
                    }
                }

            xFrame = yFrame = 0;
            return null;
        }
        private static Bitmap GetFieldImage(Bitmap image, out int xField, out int yField)
        {
            int xFrame, yFrame;
            var frameImage = GetFrameImage(image, out xFrame, out yFrame);

            if (frameImage != null)
            {

                int xStart, xEnd, yStart, yEnd;
                xStart = xEnd = yStart = yEnd = -1;

                for (int x = 0; x < frameImage.Width && xStart == -1; x++)
                    for (int y = 0; y < frameImage.Height && xStart == -1; y++)
                    {
                        if (frameImage.GetPixel(x, y) == blackColor)
                        {
                            xStart = x;
                            yStart = y;
                        }
                    }

                for (int x = frameImage.Width - 1; x >= 0 && xEnd == -1; x--)
                    for (int y = frameImage.Height - 1; y >= 0 && xEnd == -1; y--)
                    {
                        if (frameImage.GetPixel(x, y) == blackColor)
                        {
                            xEnd = x;
                            yEnd = y;
                        }
                    }

                xField = xFrame + xStart;
                yField = yFrame + yStart;

                return (new Bitmap(frameImage)).Clone(new Rectangle(xStart, yStart, xEnd - xStart, yEnd - yStart), frameImage.PixelFormat);
            }

            xField = yField = 0;
            return null;
        }
        private static Bitmap GetScreenshot()
        {
            int screenWidth = Screen.PrimaryScreen.Bounds.Width;
            int screenHeight = Screen.PrimaryScreen.Bounds.Height;
            Bitmap screenshot = new Bitmap(screenWidth, screenHeight);
            Graphics gfx = Graphics.FromImage(screenshot);
            gfx.CopyFromScreen(Screen.PrimaryScreen.WorkingArea.Left, Screen.PrimaryScreen.WorkingArea.Top, 0, 0, new Size(screenWidth, screenHeight));

            return screenshot;
        }
    }
}