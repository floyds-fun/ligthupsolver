﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Floyds.LightUpSolver
{
    public class Solver
    {
        public List<GameAction> Solve(sbyte[,] gameField, int level = 0)
        {
            Initialize(gameField);

            // steps of solving:
            // step 0 - excluding obviously void numbers - zeros
            // step 1 - for each number that left checking if it can be solved obviously
            // step 2 - looking for crossed/empty cells, that can be lightened only one way
            // step 3 - recursive guess on numbers
            // step 4 - recursive guess on empty cells

            // step 0
            this.level = level;
            step = "zero";
            foreach (var cell in numbers)
                MarkVoid(cell.X, cell.Y);
            RemoveUsedCells();

            bool nonEmptyStep1234 = true; // to make at least one try of 1234
            while (empty.Count > 0 && nonEmptyStep1234)
            {
                nonEmptyStep1234 = false; // now we need to prove, that 1234 is not empty
                bool nonEmptyStep123 = true;

                while (empty.Count > 0 && nonEmptyStep123)
                {
                    nonEmptyStep123 = false; // need to prove, that 123 is not empty
                    bool nonEmptyStep12 = true;

                    while (empty.Count > 0 && nonEmptyStep12)
                    {
                        nonEmptyStep12 = false;

#if !DEBUG
                        numbers = numbers.OrderBy(item => item.X * item.Y * rand.Next(field.GetLength(0) * field.GetLength(0))).ToList();
                        empty = empty.OrderBy(item => item.X * item.Y * rand.Next(field.GetLength(0) * field.GetLength(0))).ToList();
#endif

                        // step 1 - for each number that left checking if it can be solved obviously
                        step = "obv";
                        foreach (var cell in numbers)
                            MarkObvious(cell.X, cell.Y);

                        // if there were some actions, a part of the cells will be removed - that can be an indicator of non-empty step
                        nonEmptyStep12 |= RemoveUsedCells();

                        // step 2 - looking for crossed/empty cells, that can be lightened only one way
                        step = "only";
                        foreach (var cell in empty)
                        {
                            if (field[cell.X, cell.Y] == FieldValue.Cross || field[cell.X, cell.Y] == FieldValue.Empty)
                            {
                                var theOnly = GetTheOnlyPossibleCell(cell.X, cell.Y);
                                if (theOnly != null)
                                    PutLamp(theOnly.Value.X, theOnly.Value.Y);
                            }
                        }

                        nonEmptyStep12 |= RemoveUsedCells();
                    }

                    // step 3 - recursive guess on numbers
                    step = "guess numb";
                    if (numbers.Count > 0)
                    {
                        var firstEmpty = GetFirstEmptyCellAround(numbers[0].X, numbers[0].Y);
                        MakeRecursiveGuess(firstEmpty.X, firstEmpty.Y);

                        // we've come either to fail and have put cross
                        // or to success and have copied all child actions
                        // anyway, we need to proceed one more time
                        nonEmptyStep123 = true;
                    }

                    nonEmptyStep123 |= RemoveUsedCells() || nonEmptyStep12;
                }

                // step 4 - recursive guess on empty cells
                step = "guess empt";
                foreach (var cell in empty)
                {
                    if (field[cell.X, cell.Y] != FieldValue.Empty)
                        continue;

                    MakeRecursiveGuess(cell.X, cell.Y);
                    nonEmptyStep1234 = true;

                    // after 1 attempt we need to try easier steps
                    break;
                }

                nonEmptyStep1234 |= RemoveUsedCells() || nonEmptyStep123;
            }

            if ((empty.Count > 0 || numbers.Count > 0) && level != 0)
                throw new SolvingException("Can't solve");

            return actions;
        }
        private void MakeRecursiveGuess(int x, int y)
        {
            // making backup of critical data
            var fieldBackup = (sbyte[,])field.Clone();
            var actionsBackup = new List<GameAction>(actions);
            var numbersBackup = new List<Point>(numbers);
            var emptyBackup = new List<Point>(empty);

            // putting guess lamp
            PutLamp(x, y);

            try
            {
                // trying to solve the field in child solver
                var childSolver = new Solver();
                var childActions = childSolver.Solve(GetPurifiedField(), level + 1);

                // if we come here, the solution is found in child solver
                // copying all child actions to current actions list
                foreach (var action in childActions)
                    actions.Add(action);

                // clearing all collections to finish solving on current level - they are not needed anymore
                numbers.Clear();
                empty.Clear();
            }
            catch (SolvingException)
            {
                // restoring critical data
                field = fieldBackup;
                actions = actionsBackup;
                numbers = numbersBackup;
                empty = emptyBackup;
                removeNumbers.Clear();
                removeEmpty.Clear();

                // marking current guess as wrong
                step = "wrong guess";
                PutCross(x, y);
            }
        }

        private void Initialize(sbyte[,] gameField)
        {
            actions = new List<GameAction>();
            numbers = new List<Point>();
            empty = new List<Point>();
            removeNumbers = new List<Point>();
            removeEmpty = new List<Point>();

            // adding double block border to avoid range checking
            int dimension = gameField.GetLength(0) + 4;
            field = new sbyte[dimension, dimension];

            for (int xy = 0; xy < dimension; xy++)
            {
                field[xy, 0] = FieldValue.Block;
                field[xy, dimension - 1] = FieldValue.Block;
                field[0, xy] = FieldValue.Block;
                field[dimension - 1, xy] = FieldValue.Block;

                field[xy, 1] = FieldValue.Block;
                field[xy, dimension - 2] = FieldValue.Block;
                field[1, xy] = FieldValue.Block;
                field[dimension - 2, xy] = FieldValue.Block;
            }

            for (int x = 0; x < gameField.GetLength(0); x++)
                for (int y = 0; y < gameField.GetLength(1); y++)
                    field[x + 2, y + 2] = gameField[x, y];


            for (int x = 0; x < field.GetLength(0); x++)
                for (int y = 0; y < field.GetLength(1); y++)
                {
                    if (IsNumber(x, y))
                        numbers.Add(new Point(x, y));
                    if (field[x, y] == FieldValue.Empty || field[x, y] == FieldValue.Cross)
                        empty.Add(new Point(x, y));
                }
        }
        private sbyte[,] GetPurifiedField()
        {
            var result = new sbyte[field.GetLength(0) - 4, field.GetLength(1) - 4];
            for (int x = 0; x < field.GetLength(0) - 4; x++)
                for (int y = 0; y < field.GetLength(1) - 4; y++)
                    result[x, y] = field[x + 2, y + 2];

            return result;
        }
        private bool RemoveUsedCells()
        {
            bool result = removeEmpty.Count > 0 || removeNumbers.Count > 0;

            foreach (var pp in removeEmpty)
                empty.Remove(pp);

            foreach (var pp in removeNumbers)
                numbers.Remove(pp);

            removeEmpty.Clear();
            removeNumbers.Clear();

            return result;
        }

        private byte GetLampsCount(int x, int y)
        {
            byte result = 0;
            if (field[x + 1, y] == FieldValue.Lamp)
                result++;
            if (field[x, y + 1] == FieldValue.Lamp)
                result++;
            if (field[x - 1, y] == FieldValue.Lamp)
                result++;
            if (field[x, y - 1] == FieldValue.Lamp)
                result++;

            return result;
        }
        private byte GetEmptyCount(int x, int y)
        {
            byte result = 0;
            if (field[x + 1, y] == FieldValue.Empty)
                result++;
            if (field[x, y + 1] == FieldValue.Empty)
                result++;
            if (field[x - 1, y] == FieldValue.Empty)
                result++;
            if (field[x, y - 1] == FieldValue.Empty)
                result++;

            return result;
        }
        private Point? GetTheOnlyPossibleCell(int x, int y)
        {
            Point? theOnly = null;
            if (field[x, y] == FieldValue.Empty)
                theOnly = new Point(x, y);

            for (int xx = x + 1, yy = y; IsTransperent(xx, yy); xx++)
            {
                if (field[xx, yy] == FieldValue.Empty)
                    if (theOnly == null)
                        theOnly = new Point(xx, yy);
                    else
                        return null;
            }
            for (int xx = x, yy = y + 1; IsTransperent(xx, yy); yy++)
            {
                if (field[xx, yy] == FieldValue.Empty)
                    if (theOnly == null)
                        theOnly = new Point(xx, yy);
                    else
                        return null;
            }
            for (int xx = x - 1, yy = y; IsTransperent(xx, yy); xx--)
            {
                if (field[xx, yy] == FieldValue.Empty)
                    if (theOnly == null)
                        theOnly = new Point(xx, yy);
                    else
                        return null;
            }
            for (int xx = x, yy = y - 1; IsTransperent(xx, yy); yy--)
            {
                if (field[xx, yy] == FieldValue.Empty)
                    if (theOnly == null)
                        theOnly = new Point(xx, yy);
                    else
                        return null;
            }

            if (theOnly == null)
                throw new SolvingException("Can't light a cell");

            return theOnly;
        }
        private Point GetFirstEmptyCellAround(int x, int y)
        {
            if (IsNumber(x, y) && GetLampsCount(x, y) < field[x, y])
            {
                if (field[x + 1, y] == FieldValue.Empty)
                    return new Point(x + 1, y);
                if (field[x, y + 1] == FieldValue.Empty)
                    return new Point(x, y + 1);
                if (field[x - 1, y] == FieldValue.Empty)
                    return new Point(x - 1, y);
                if (field[x, y - 1] == FieldValue.Empty)
                    return new Point(x, y - 1);

                throw new SolvingException("Cant find empty cell");
            }

            throw new Exception("Can call this method only for unsolved number cells.");
        }

        private bool IsNumber(int x, int y)
        {
            return field[x, y] >= FieldValue.Zero && field[x, y] <= FieldValue.Four;
        }
        private bool IsTransperent(int x, int y)
        {
            return field[x, y] == FieldValue.Empty || field[x, y] == FieldValue.Cross || field[x, y] == FieldValue.Light;
        }
        private bool IsObviousVoid(int x, int y)
        {
            return field[x, y] == GetLampsCount(x, y);
        }
        private bool IsObviousLamp(int x, int y)
        {
            return field[x, y] != FieldValue.Zero && field[x, y] - GetLampsCount(x, y) == GetEmptyCount(x, y);
        }

        private void MarkVoid(int x, int y)
        {
            if (IsObviousVoid(x, y))
            {
                PutCross(x + 1, y);
                PutCross(x, y + 1);
                PutCross(x - 1, y);
                PutCross(x, y - 1);

                removeNumbers.Add(new Point(x, y));
            }
        }
        private void MarkObvious(int x, int y)
        {
            if (IsObviousLamp(x, y))
            {
                PutLamp(x + 1, y);
                PutLamp(x, y + 1);
                PutLamp(x - 1, y);
                PutLamp(x, y - 1);

                if (field[x, y] - GetLampsCount(x, y) > GetEmptyCount(x, y))
                    throw new SolvingException("Not enough space for lamps");

                removeNumbers.Add(new Point(x, y));
            }
            else
            {
                if (field[x, y] - GetLampsCount(x, y) > GetEmptyCount(x, y))
                    throw new SolvingException("Not enough space for lamps");
            }
        }

        private void PutCross(int x, int y)
        {
            if (field[x, y] != FieldValue.Empty)
                return;

            field[x, y] = FieldValue.Cross;
            actions.Add(new GameAction(false, x - 2, y - 2, GetComment()));
        }
        private void PutLamp(int x, int y, bool markVoidNeighbours = true)
        {
            if (field[x, y] != FieldValue.Empty)
                return;

            for (int xx = x + 1, yy = y; IsTransperent(xx, yy); xx++)
            {
                if (field[xx, yy] == FieldValue.Lamp)
                    throw new SolvingException("Lamp on the way!");

                field[xx, yy] = FieldValue.Light;
                removeEmpty.Add(new Point(xx, yy));
            }
            for (int xx = x, yy = y + 1; IsTransperent(xx, yy); yy++)
            {
                if (field[xx, yy] == FieldValue.Lamp)
                    throw new SolvingException("Lamp on the way!");

                field[xx, yy] = FieldValue.Light;
                removeEmpty.Add(new Point(xx, yy));
            }
            for (int xx = x - 1, yy = y; IsTransperent(xx, yy); xx--)
            {
                if (field[xx, yy] == FieldValue.Lamp)
                    throw new SolvingException("Lamp on the way!");

                field[xx, yy] = FieldValue.Light;
                removeEmpty.Add(new Point(xx, yy));
            }
            for (int xx = x, yy = y - 1; IsTransperent(xx, yy); yy--)
            {
                if (field[xx, yy] == FieldValue.Lamp)
                    throw new SolvingException("Lamp on the way!");

                field[xx, yy] = FieldValue.Light;
                removeEmpty.Add(new Point(xx, yy));
            }

            field[x, y] = FieldValue.Lamp;
            removeEmpty.Add(new Point(x, y));

            actions.Add(new GameAction(true, x - 2, y - 2, GetComment()));

            if (markVoidNeighbours)
            {
                MarkVoid(x + 1, y);
                MarkVoid(x, y + 1);
                MarkVoid(x - 1, y);
                MarkVoid(x, y - 1);
            }
        }
        private string GetComment()
        {
            return "level #" + level + ", " + step;
        }

        public static sbyte[,] ApplyGameAction(sbyte[,] gameField, GameAction action)
        {
            littleSlave.Initialize(gameField);
            if (action.IsLamp)
                littleSlave.PutLamp(action.X + 2, action.Y + 2, false);
            else
                littleSlave.PutCross(action.X + 2, action.Y + 2);

            return littleSlave.GetPurifiedField();
        }

        private string FieldView
        {
            get
            {
                string result = GetComment() + Environment.NewLine;
                for (int y = 1; y < field.GetLength(1) - 1; y++)
                {
                    for (int x = 1; x < field.GetLength(0) - 1; x++)
                    {
                        switch (field[x, y])
                        {
                            case FieldValue.Zero:
                            case FieldValue.One:
                            case FieldValue.Two:
                            case FieldValue.Three:
                            case FieldValue.Four:
                                result += field[x, y];
                                break;
                            case FieldValue.Block:
                                result += "■";
                                break;
                            case FieldValue.Cross:
                                result += "x";
                                break;
                            case FieldValue.Lamp:
                                result += "©";
                                break;
                            case FieldValue.Light:
                                result += "□";
                                break;
                            default:
                                result += " ";
                                break;
                        }

                        result += " ";
                    }

                    result += Environment.NewLine;
                }

                return result;
            }
        }

        private sbyte[,] field;
        private List<Point> numbers;
        private List<Point> empty;

        private List<Point> removeNumbers;
        private List<Point> removeEmpty;

        private List<GameAction> actions;
        private int level;
        private string step;

        private static Solver littleSlave = new Solver();
        private static Random rand = new Random();
    }
}