﻿namespace Floyds.LightUpSolver
{
    public class GameAction
    {
        public GameAction(bool isLamp, int x, int y, string comment = "")
        {
            IsLamp = isLamp;
            X = x;
            Y = y;
            Comment = comment;
        }

        public bool IsLamp { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public string Comment { get; set; }
    }
}