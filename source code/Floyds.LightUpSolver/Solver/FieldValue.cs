﻿namespace Floyds.LightUpSolver
{
    public static class FieldValue
    {
        public const sbyte Block = -1;
        public const sbyte Zero = 0;
        public const sbyte One = 1;
        public const sbyte Two = 2;
        public const sbyte Three = 3;
        public const sbyte Four = 4;
        public const sbyte Empty = 5;
        public const sbyte Cross = 6;
        public const sbyte Light = 7;
        public const sbyte Lamp = 8;
    }
}