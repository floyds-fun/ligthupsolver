﻿using System;

namespace Floyds.LightUpSolver
{
    public class SolvingException : Exception
    {
        public SolvingException(string message)
            : base(message)
        {
        }
    }
}