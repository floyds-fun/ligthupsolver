﻿using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Floyds.LightUpSolver
{
    public static class MouseController
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        private const uint MOUSEEVENTF_LEFTDOWN = 0x02;
        private const uint MOUSEEVENTF_LEFTUP = 0x04;
        private const uint MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const uint MOUSEEVENTF_RIGHTUP = 0x10;

        public static void DoLeftClick(int x, int y)
        {
            var prevPosition = Cursor.Position;
            Cursor.Position = new Point(x, y);

            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)Cursor.Position.X, (uint)Cursor.Position.Y, 0, 0);

            Cursor.Position = prevPosition;
        }
        public static void DoRightClick(int x, int y)
        {
            var prevPosition = Cursor.Position;
            Cursor.Position = new Point(x, y);

            mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, (uint)Cursor.Position.X, (uint)Cursor.Position.Y, 0, 0);

            Cursor.Position = prevPosition;
        }
    }
}