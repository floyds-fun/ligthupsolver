﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Floyds.LightUpSolver
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        #region Logic Methods

        private bool LoadField()
        {
            Stopwatch sw = Stopwatch.StartNew();

            field = null;
            actions = null;
            prevField = null;

            try
            {
                field = prevField = InputParser.GetField(out fieldX, out fieldY);
            }
            catch (Exception ex)
            {
                this.Text = ex.Message;
            }

            sw.Stop();

            if (field != null)
            {
                this.Text = "Loaded in " + sw.Elapsed.ToString(@"ss\.fff");
                this.Width = field.GetLength(0) * Global.GridSize * 2 + btnLoad.Width + Global.GridSize * 3;
                this.Height = field.GetLength(1) * Global.GridSize + 41;
            }

            btnSolve.Enabled = field != null;
            btnNext.Enabled = false;
            btnApply.Enabled = false;

            ClearForm();
            DrawField();

            return field != null;
        }
        private bool Solve()
        {
            if (field != null)
            {
                Stopwatch sw = Stopwatch.StartNew();

                var solver = new Solver();
                actions = solver.Solve(field);
                solution = new List<GameAction>(actions);

                sw.Stop();
                this.Text = "Solved in " + sw.Elapsed.ToString(@"ss\.fff");
                prevStepText = "empty";

                btnSolve.Enabled = false;
                btnNext.Enabled = true;
                btnApply.Enabled = true;
            }

            return solution.Count > 0;
        }
        private void NextStep()
        {
            if (actions != null && actions.Count > 0)
            {
                var action = actions[0];

                prevField = field;
                field = Solver.ApplyGameAction(field, action);
                this.Text = prevStepText + " -> " + (action.IsLamp ? "lamp at [" : "cross at [") + action.X + "," + action.Y + "] (" + action.Comment + ")";
                prevStepText = (action.IsLamp ? "lamp at [" : "cross at [") + action.X + "," + action.Y + "]";
                actions.RemoveAt(0);

                btnNext.Enabled = actions.Count > 0;

                DrawField();
            }
        }
        private void ApplySolution()
        {
            // removing some crosses not to reveal AI solution
            var crossesToRemove = new List<GameAction>();
            var rand = new Random();
            var zeroCrossSequence = true;
            var fieldSize = field.GetLength(0);

            foreach (var action in solution)
            {
                if (!action.IsLamp && (zeroCrossSequence || rand.Next(fieldSize / 14 + 1) == 0))
                    crossesToRemove.Add(action);

                zeroCrossSequence &= !action.IsLamp;
            }
            foreach (var action in crossesToRemove)
                solution.Remove(action);

            var totalTime = ((double)fieldSize * fieldSize / 10 - (double)fieldSize / 2) * 1000 - (fieldSize / 13) * 1000; // in ms
            var singleTime = (int)totalTime / solution.Count;
            var fluctTime = singleTime / 4;

            // clicking with some lag
            foreach (var action in solution)
            {
                int realX = fieldX + action.X * Global.GridSize + 10;
                int realY = fieldY + action.Y * Global.GridSize + 10;

                if (action.IsLamp)
                    MouseController.DoLeftClick(realX, realY);
                else
                    MouseController.DoRightClick(realX, realY);

                Thread.Sleep(singleTime + Math.Sign(rand.Next(2) - 0.99) * rand.Next(fluctTime));
            }

            btnApply.Enabled = false;
        }
        private void InstantSolve()
        {
            if (LoadField() && Solve())
                ApplySolution();
        }

        #endregion

        #region Drawing

        private Bitmap DrawFieldImage(sbyte[,] field)
        {
            Font gridFont = new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold);

            Bitmap fieldImage = new Bitmap(field.GetLength(0) * Global.GridSize + 1, field.GetLength(1) * Global.GridSize + 1);
            Graphics graph = Graphics.FromImage(fieldImage);
            for (int x = 0; x < field.GetLength(0); x++)
                for (int y = 0; y < field.GetLength(1); y++)
                {
                    graph.DrawRectangle(Pens.Black, x * Global.GridSize, y * Global.GridSize, Global.GridSize, Global.GridSize);

                    switch (field[x, y])
                    {
                        case FieldValue.Block:
                            graph.FillRectangle(Brushes.Black, x * Global.GridSize, y * Global.GridSize, Global.GridSize, Global.GridSize);
                            break;
                        case FieldValue.Zero:
                        case FieldValue.One:
                        case FieldValue.Two:
                        case FieldValue.Three:
                        case FieldValue.Four:
                            graph.FillRectangle(Brushes.Black, x * Global.GridSize, y * Global.GridSize, Global.GridSize, Global.GridSize);
                            graph.DrawString(field[x, y].ToString(), gridFont, Brushes.White, x * Global.GridSize + 3, y * Global.GridSize + 1);
                            break;
                        case FieldValue.Empty:
                            graph.FillRectangle(Brushes.White, x * Global.GridSize + 1, y * Global.GridSize + 1, Global.GridSize - 1, Global.GridSize - 1);
                            break;
                        case FieldValue.Cross:
                            var crossPen = new Pen(Color.Red, 2);
                            graph.FillRectangle(Brushes.White, x * Global.GridSize + 1, y * Global.GridSize + 1, Global.GridSize - 1, Global.GridSize - 1);
                            graph.DrawLine(crossPen, x * Global.GridSize + 5, y * Global.GridSize + 5, (x + 1) * Global.GridSize - 5, (y + 1) * Global.GridSize - 5);
                            graph.DrawLine(crossPen, x * Global.GridSize + 5, (y + 1) * Global.GridSize - 5, (x + 1) * Global.GridSize - 5, y * Global.GridSize + 5);
                            break;
                        case FieldValue.Light:
                            graph.FillRectangle(Brushes.Khaki, x * Global.GridSize + 1, y * Global.GridSize + 1, Global.GridSize - 1, Global.GridSize - 1);
                            break;
                        case FieldValue.Lamp:
                            graph.FillRectangle(Brushes.Khaki, x * Global.GridSize + 1, y * Global.GridSize + 1, Global.GridSize - 1, Global.GridSize - 1);
                            graph.FillRectangle(Brushes.DarkSlateGray, x * Global.GridSize + 7, y * Global.GridSize + 10, 4, 5);
                            graph.FillEllipse(Brushes.Yellow, x * Global.GridSize + 4, y * Global.GridSize + 3, Global.GridSize - 9, Global.GridSize - 9);
                            graph.DrawEllipse(Pens.Black, x * Global.GridSize + 4, y * Global.GridSize + 3, Global.GridSize - 9, Global.GridSize - 9);
                            break;
                    }
                }

            return fieldImage;
        }
        private void DrawField()
        {
            var graph = this.CreateGraphics();

            if (field != null)
            {
                graph.DrawImage(DrawFieldImage(prevField), 1, 1);
                graph.DrawImage(DrawFieldImage(field), field.GetLength(0) * Global.GridSize + Global.GridSize, 1);
            }
        }
        private void ClearForm()
        {
            this.CreateGraphics().Clear(SystemColors.Control);
        }

        #endregion

        #region Service

        private void RegisterHotKey()
        {
            try
            {
                var instantSolveHook = new KeyboardHook();
                instantSolveHook.KeyPressed += instantSolveHook_KeyPressed;
                instantSolveHook.RegisterHotKey(Floyds.LightUpSolver.ModifierKeys.Control, Keys.F12);
            }
            catch
            {
                MessageBox.Show("Hotkey was not registered.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Event Handlers

        private void btnLoad_Click(object sender, System.EventArgs e)
        {
            LoadField();
        }
        private void btnSolve_Click(object sender, System.EventArgs e)
        {
            Solve();
        }
        private void btnNext_Click(object sender, System.EventArgs e)
        {
            NextStep();
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            ApplySolution();
        }
        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            DrawField();
        }
        private void FormMain_Load(object sender, EventArgs e)
        {
            RegisterHotKey();
        }
        private void instantSolveHook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            InstantSolve();
        }

        #endregion

        private sbyte[,] field;
        private sbyte[,] prevField;
        private List<GameAction> actions;
        private List<GameAction> solution;
        private string prevStepText;
        private int fieldX;
        private int fieldY;
    }
}