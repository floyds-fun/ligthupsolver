﻿using System;
using System.Windows.Forms;

namespace Floyds.LightUpSolver
{
    public partial class FormSplash : Form
    {
        public FormSplash()
        {
            InitializeComponent();
        }

        private void FormSplash_Shown(object sender, EventArgs e)
        {
            this.SetTopLevel(true);
            timerStartup.Enabled = true;
        }

        private void timerStartup_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}