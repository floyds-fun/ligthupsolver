# LigthUp Solver

Solver of Light Up puzzle game which can be found at https://www.puzzle-light-up.com.

Program parses game field from the screen, solves the puzzle and clicks back the solution.

Originally designed for Mozilla Firefox browser in 2016. Now I plan to adapt it to any browser.